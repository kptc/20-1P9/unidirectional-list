from mylib.item import Item


class MyList:
    def __init__(self):
        self.head = None

    def append(self, value):
        if self.head is None:
            self.head = Item(value)
            return

        tmp = self.head
        while tmp.Next:
            tmp = tmp.Next

        tmp.Next = Item(value)
