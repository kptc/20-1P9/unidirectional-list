import pytest

from mylib.mylist import MyList


def test_1():
    my_list = MyList()
    my_list.append(2)
    assert my_list.head is not None, "В список не был добавлен новый элемент"
    assert my_list.head.value == 2, "В списке хранится неизвестное значение"
